/*
*   $ cd /node_modules/mocha/bin
*   $ ./mocha ../../../test/
*/

var describe = require("mocha").describe;
var expect = require("chai").expect;
var request = require("request");

describe("Upload functionality:", function(){

    it ("S3 upload page status", function (done) {
        request("http://0.0.0.0:3001/home", function (error, response, body) {
            expect(response.statusCode).to.equal(200);
            done();
        });
    });

    it ("Uploading image to S3 status", function (done) {
        request("http://0.0.0.0:3001/successful-upload", function (error, response, body) {
            expect(response.statusCode).to.equal(200);
            done();
        });
    });

});
